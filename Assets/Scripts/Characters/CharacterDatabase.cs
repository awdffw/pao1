﻿using UnityEngine;
using AssetBundles;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 当前角色存储脚本,以角色名字索引
/// </summary>
public class CharacterDatabase
{
    /// <summary>
    /// 角色字典
    /// </summary>
    static protected Dictionary<string, Character> m_CharactersDict;

    static public Dictionary<string, Character> Dictionary {  get { return m_CharactersDict; } }

    static protected bool m_Loaded = false;
    static public bool loaded { get { return m_Loaded; } }

    /// <summary>
    /// 获取角色
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    static public Character GetCharacter(string type)
    {
        Character c;
        if (m_CharactersDict == null || !m_CharactersDict.TryGetValue(type, out c))
            return null;

        return c;
    }

    /// <summary>
    /// 异步加载预制体
    /// </summary>
    /// <param name="packages"></param>
    /// <returns></returns>
    static public IEnumerator LoadDatabase(List<string> packages)
    {
        if (m_CharactersDict == null)
        {
            m_CharactersDict = new Dictionary<string, Character>();

            foreach (string s in packages)
            {


                AssetBundleLoadAssetOperation op = AssetBundleManager.LoadAssetAsync(s, "character", typeof(GameObject));

                yield return CoroutineHandler.StartStaticCoroutine(op);
                /// 获取角色挂载的character脚本
                Character c = op.GetAsset<GameObject>().GetComponent<Character>();
                if (c != null)
                {
                    m_CharactersDict.Add(c.characterName, c);
                }
            }

            m_Loaded = true;
        }
    }
}