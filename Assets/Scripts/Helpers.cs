﻿using UnityEngine;

public class Helpers
{
    // This sets the layer of all renderer children of a given gameobject (including itself)
    // Useful to make an object display only on a single camera (e.g. character on UI cam on loadout State)
    //这设置了给定游戏对象(包括自身)所有呈现器的子层的层
    //有用的是使一个对象只显示在一个相机上(例如，在加载状态下的UI凸轮上的字符)
    static public void SetRendererLayerRecursive(GameObject root, int layer)
    {
        Renderer[] rends = root.GetComponentsInChildren<Renderer>(true);

        for(int i = 0; i < rends.Length; ++i)
        {
            rends[i].gameObject.layer = layer;
        }
    }
}
