﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssetBundles;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// This class handles listing the bundles and distributing
/// them to the databases to load.
/// 这个类将处理打包和分发的包
/// 它们到数据库加载。
/// </summary>
public class AssetBundlesDatabaseHandler
{
    static public void Load()
    {
        CoroutineHandler.StartStaticCoroutine(AsyncLoad());
    }
    /// <summary>
    /// 异步将AssetBundle中的预制体解压出来
    /// </summary>
    /// <returns></returns>
    static IEnumerator AsyncLoad()
    {

        // Android store streams assets in a compressed archive, so different file system.
        AssetBundleManager.BaseDownloadingURL = "file://" + Application.streamingAssetsPath + "/AssetBundles/" + Utility.GetPlatformName() + "/";
        var request = AssetBundleManager.Initialize();
        if (request != null)
            yield return CoroutineHandler.StartStaticCoroutine(request);

        // In editor we can directly get all the bundles but in final build, we need to read them from the manifest.
        string[] bundles;
        if(AssetBundleManager.SimulateAssetBundleInEditor)
            bundles = AssetDatabase.GetAllAssetBundleNames();
        else
            bundles = AssetBundleManager.AssetBundleManifestObject.GetAllAssetBundles();

        List<string> characterPackage = new List<string>();
        List<string> themePackage = new List<string>();
        //添加bundles中的组件
        for (int i = 0; i < bundles.Length; ++i)
        {

            if (bundles[i].StartsWith("characters/"))
            {
                characterPackage.Add(bundles[i]);
            }
            else if (bundles[i].StartsWith("themes/"))
                themePackage.Add(bundles[i]);
        }

        yield return CoroutineHandler.StartStaticCoroutine(CharacterDatabase.LoadDatabase(characterPackage));
        yield return CoroutineHandler.StartStaticCoroutine(ThemeDatabase.LoadDatabase(themePackage));
    }
}
